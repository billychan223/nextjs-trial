module.exports = {
  env: {
    DB_CONNECTION: "",
    DB_URI: "",

    STRIPE_API_KEY: "",
    STRIPE_SECRET_KEY: "",

    STRIPE_WEBHOOK_SECRET: "",

    IMAGE_CLOUD_NAME: "",
    IMAGE_API_KEY: "",
    IMAGE_API_SECRET: "",

    SMTP_HOST: "",
    SMTP_PORT: "",
    SMTP_USER: "",
    SMTP_PASSWORD: "",
    SMTP_FROM_EMAIL: "",
    SMTP_FROM_NAME: "",

    NEXTAUTH_URL: "",
  },
  images: {
    domains: ["res.cloudinary.com"],
  },
};
